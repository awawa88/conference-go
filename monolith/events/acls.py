import json
import requests
from .keys import PEXELS_API_KEY, OPENWEATHER_API_KEY



def get_photo(city, state):
    url = "https://api.pexels.com/v1/search"
    params = {"per_page": 1, "query": city + " " + state}
    headers = {"Authorization": PEXELS_API_KEY}

    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)
    try:

        return {"picture_url": content["photos"][0]["src"]["original"]}
    except {KeyError, IndexError}:
        return {"picture_url": None}

def get_weather(city, state):
    url = "https://api.openweathermap.org/geo/1.0/direct"
    params = {
        "q": city + "," + state,
        "appid": OPENWEATHER_API_KEY,

    }

    response = requests.get(url, params=params)
    location = json.loads(response.content)
    lat = location["lat"]
    lon = location['lon']

    url2 = "https://api.openweathermap.org/data/2.5/weather"
    params2 = {
        "lat": lat,
        "lon": lon,
        "appid": OPENWEATHER_API_KEY
    }

    response2 = requests.get(url2, params=params2)
    content = json.loads(response2.content)

    return {
        "temp": content["main"]["temp"],
        "weather_description": content["weather"][0]["description"]
    }
